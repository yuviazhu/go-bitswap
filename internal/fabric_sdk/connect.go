package fabric_sdk

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

type Client struct {
	Context       *context.ClientProvider
	ResMgmtClient *resmgmt.Client
	ChannelClient *channel.Client
	Peer          string
	Identity      string
}

const ChannelID = "mychannel"

// 启动sdk
func SetUp(configFile string, client *Client) (*fabsdk.FabricSDK, error) {
	// 获得sdk
	sdk, err := fabsdk.New(config.FromFile(configFile))
	if err != nil {
		return nil, err
	}
	// 获得org1，user1用户的客户端上下文
	context := sdk.Context(fabsdk.WithUser("User1"), fabsdk.WithOrg("Org1"))
	client.Context = &context

	// 获得资源管理客户端
	resMgmtClient, err := resmgmt.New(context)
	if err != nil {
		return nil, fmt.Errorf("创建资源管理客户端失败: %v", err)
	}
	client.ResMgmtClient = resMgmtClient

	// 获得通道客户端
	channelContext := sdk.ChannelContext(ChannelID, fabsdk.WithUser("User1"), fabsdk.WithOrg("Org1"))
	channelClient, err := channel.New(channelContext)
	if err != nil {
		return nil, fmt.Errorf("创建通道客户端失败: %v", err)
	}
	client.ChannelClient = channelClient
	client.Peer = "peer0.org1.example.com"
	client.Identity = "User1@org1.example.com"

	return sdk, nil
}
