package fabric_sdk

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// sdk客户端
var client Client
var sdk *fabsdk.FabricSDK

// 查询是否有权限
func (c *Client) CheckAuthority(dirCID string, peerId string, authority int) (string, error) {
	var args [][]byte
	args = append(args, []byte(dirCID), []byte(fmt.Sprint(peerId)), []byte(fmt.Sprint(authority)))

	req := channel.Request{
		ChaincodeID: "authority",
		Fcn:         "CheckUserAuthority",
		Args:        args,
	}
	response, err := c.ChannelClient.Query(req)
	if err != nil {
		return "", err
	}
	sdk.Close()
	// 返回负载
	return string(response.Payload), nil
}
