package decision

import (
	"context"
	"fmt"
	"sync"

	blocks "github.com/ipfs/go-block-format"
	cid "github.com/ipfs/go-cid"
	bstore "github.com/ipfs/go-ipfs-blockstore"
	"github.com/ipfs/go-metrics-interface"
	process "github.com/jbenet/goprocess"
)

// blockstoreManager maintains a pool of workers that make requests to the blockstore.
type blockstoreManager struct {
	bs           bstore.Blockstore
	workerCount  int
	jobs         chan func()
	px           process.Process
	pendingGauge metrics.Gauge
	activeGauge  metrics.Gauge
}

// newBlockstoreManager creates a new blockstoreManager with the given context
// and number of workers
func newBlockstoreManager(
	ctx context.Context,
	bs bstore.Blockstore,
	workerCount int,
	pendingGauge metrics.Gauge,
	activeGauge metrics.Gauge,
) *blockstoreManager {
	return &blockstoreManager{
		bs:           bs,
		workerCount:  workerCount,
		jobs:         make(chan func()),
		px:           process.WithTeardown(func() error { return nil }),
		pendingGauge: pendingGauge,
		activeGauge:  activeGauge,
	}
}

// 开启bsm
func (bsm *blockstoreManager) start(px process.Process) {
	// 设置px为bsm进程的父进程
	px.AddChild(bsm.px)
	// Start up workers
	for i := 0; i < bsm.workerCount; i++ {
		// 创建多个子进程作为工作进程，每个都执行worker函数
		bsm.px.Go(func(px process.Process) {
			bsm.worker(px)
		})
	}
}

// 统计job数量
func (bsm *blockstoreManager) worker(px process.Process) {
	for {
		select {
		case <-px.Closing():
			// 如果结束了则返回
			return
		case job := <-bsm.jobs:
			// 有任务，则由bsm进程执行统计，活动+1，挂起-1，执行，活动-1
			bsm.pendingGauge.Dec()
			bsm.activeGauge.Inc()
			job()
			bsm.activeGauge.Dec()
		}
	}
}

func (bsm *blockstoreManager) addJob(ctx context.Context, job func()) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-bsm.px.Closing():
		return fmt.Errorf("shutting down")
	case bsm.jobs <- job:
		bsm.pendingGauge.Inc()
		return nil
	}
}

// 返回每个请求的cid对应的块大小
func (bsm *blockstoreManager) getBlockSizes(ctx context.Context, ks []cid.Cid) (map[cid.Cid]int, error) {
	res := make(map[cid.Cid]int)
	if len(ks) == 0 {
		return res, nil
	}

	var lk sync.Mutex
	// 对每个cid执行getsize，并修改res，即返回值
	return res, bsm.jobPerKey(ctx, ks, func(c cid.Cid) {
		size, err := bsm.bs.GetSize(ctx, c)
		if err != nil {
			if err != bstore.ErrNotFound {
				// Note: this isn't a fatal error. We shouldn't abort the request
				log.Errorf("blockstore.GetSize(%s) error: %s", c, err)
			}
		} else {
			lk.Lock()
			res[c] = size
			lk.Unlock()
		}
	})
}

func (bsm *blockstoreManager) getBlocks(ctx context.Context, ks []cid.Cid) (map[cid.Cid]blocks.Block, error) {
	res := make(map[cid.Cid]blocks.Block)
	if len(ks) == 0 {
		return res, nil
	}

	var lk sync.Mutex
	return res, bsm.jobPerKey(ctx, ks, func(c cid.Cid) {
		blk, err := bsm.bs.Get(ctx, c)
		if err != nil {
			if err != bstore.ErrNotFound {
				// Note: this isn't a fatal error. We shouldn't abort the request
				log.Errorf("blockstore.Get(%s) error: %s", c, err)
			}
		} else {
			lk.Lock()
			res[c] = blk
			lk.Unlock()
		}
	})
}

// 对每个cid执行job
func (bsm *blockstoreManager) jobPerKey(ctx context.Context, ks []cid.Cid, jobFn func(c cid.Cid)) error {
	var err error
	wg := sync.WaitGroup{}
	for _, k := range ks {
		// 对于每个cid，并将其添加为job，执行jobFn
		c := k
		wg.Add(1)
		err = bsm.addJob(ctx, func() {
			jobFn(c)
			wg.Done()
		})
		if err != nil {
			wg.Done()
			break
		}
	}
	// 等待wg中每个成员完成jobFn
	wg.Wait()
	return err
}
