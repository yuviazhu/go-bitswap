package decision

import intdec "gitee.com/yuviazhu/go-bitswap/internal/decision"

// Expose Receipt externally
type Receipt = intdec.Receipt

// Expose ScoreLedger externally
type ScoreLedger = intdec.ScoreLedger

// Expose ScorePeerFunc externally
type ScorePeerFunc = intdec.ScorePeerFunc
